<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Toy Shop</title>

	<style type="text/css">

#header {
	background-color: #F4A460;
	margin: 0;
  	padding: 0;
}

#navbar {
  list-style-type: none;
}

#navbar li { display: inline; }

#navbar a {
  color: #fff;
  padding: 5px 10px;
  text-decoration: none;
  font-weight: bold;
  display: inline-block;
  width: 100px;
}



#footer {margin: 0;
  	padding: 0;
	background-color: #A9A9A9;} 

#footer p {
  color: #fff;
  padding: 5px 10px;
  font-weight: bold;
  display: inline-block;
  width: 100px;
}
	</style>
</head>
<body>
<div id="wrap">
	<div id="header">
		<ul id="navbar">
		  <li><a href="/">Main</a></li>
		  <li><a href="/articles">Articles</a></li>
		  <li><a href="/shop">Shop</a></li>
		  <li><a href="/contacts">Contacts</a></li>
		</ul>
  	</div>

<div id="container">

	</div>
	</div>
</div>
<div id="footer">
	<p>
		Created by Lera
	</p>
</div>
</body>
</html>