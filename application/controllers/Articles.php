<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {

	public function index() {
		$this->load->model('articles_model');
		$articles = $this->articles_model->get_articles();
		$data['title'] = $articles['title'];
		$data['intro'] = $articles['intro'];
		$this->load->view('article_index', $data);
	}

	public function show($id) {
		$this->load->model('articles_model');
		$article = $this->articles_model->get_article($id);
		$data['title'] = $article['title'];
		$data['intro'] = $article['intro'];
		$this->load->view('article_intro', $data);
	}
}
?>