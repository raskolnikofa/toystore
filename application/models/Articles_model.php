<?php
class Articles_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	public function get_article($id) {
		if($id != FALSE) {
			$query = $this->db->get_where('articles', array('id' => $id));
		return $query->row_array();
		}
		else {
			return FALSE;
		}
	}

	public function get_articles() {
			$query = $this->db-> select -> ('*')->  from->('articles');
		return $query->row_array();
	}
}
?>